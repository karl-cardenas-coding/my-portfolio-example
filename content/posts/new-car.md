---
title: "New Car"
date: 2020-07-18T15:20:47-07:00
draft: true
---

# A car for everyone

Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum

### How to buy a Car
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam nisi nulla, hendrerit a massa sed, blandit aliquam enim. Suspendisse placerat convallis augue eget convallis. Pellentesque ultrices cursus pretium. Donec aliquet auctor sodales. Donec vel finibus libero, ornare commodo leo. Nullam volutpat vel mauris sit amet aliquet. Etiam ultrices arcu nec dignissim maximus. Vestibulum accumsan tellus sit amet porta mattis. Proin egestas vel dui a egestas. Phasellus semper erat orci, et feugiat dui mollis vitae.

Suspendisse auctor velit tempor felis tincidunt, at semper lacus ultrices. Vivamus ac bibendum velit, eu aliquam mauris. In bibendum, lacus id congue elementum, tortor lectus tempor massa, quis semper enim mauris facilisis elit. Maecenas a nibh maximus, tristique urna nec, venenatis ipsum. Morbi lacus tortor, sagittis vitae dapibus eget, tempus non tortor. Phasellus eros lorem, sagittis sit amet commodo vitae, lacinia sit amet odio. Aliquam erat volutpat. Maecenas tincidunt justo nec ex elementum dictum. Nulla elementum massa tortor, vel luctus lorem condimentum sed. Phasellus et tincidunt diam, at sollicitudin nisi. Fusce quis urna nisl. Curabitur ultrices risus nec hendrerit faucibus. Donec luctus eget ipsum vel rutrum. Mauris non odio enim. Mauris aliquet feugiat metus, quis auctor mi maximus id.

{{< youtube 4cGStXBAwFg >}}
